﻿using UnityEngine;
using UnityEngine.UI;

public class LivesController : MonoBehaviour {

    public Image[] liveImages;

    public void UpdateLives(int currentHealth) {
        foreach (var liveImage in liveImages) {
            liveImage.enabled = false;
        }
        for (var i = 0; i < currentHealth; i++) {
            liveImages[i].enabled = true;
        }
    }
}