﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour {

    public Animator anim;
    public void PressedStartButton() {
        anim.SetTrigger("StartGame");
    }

    public void PressedCreditsButton() {
        anim.SetTrigger("Credits");
    }

    public void Cancel() {
        anim.SetTrigger("Cancel");
    }

    public void Update() {
        if (Input.GetButtonDown("Cancel")) {
            Cancel();
        }
    }
}
