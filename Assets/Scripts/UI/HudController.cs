﻿using UnityEngine;
using UnityEngine.UI;

public class HudController : MonoBehaviour {

    public PlayerStats player;
    public LivesController lives;
    public Image currentPattern;


    void Start() {
        player.onHealthChanged += (o, e) => {
            lives.UpdateLives(player.health);
        };
        player.onPatternChanged += (o, e) => {
            currentPattern.sprite = player.currentPattern.sprite;
        };
    }

}