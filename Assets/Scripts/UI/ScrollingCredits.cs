﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingCredits : MonoBehaviour {

    public Transform target;
    public float scrollLength;
    public float scrollSpeed;
    public float waitTime;

    private float currentDistance;
    private bool isWaiting;
    private Vector3 originalPosition;

    void Start () {
	    originalPosition = target.position;
        isWaiting = false;
    }
	
	
	void Update () {
	    if (currentDistance >= scrollLength) {
	        if (isWaiting) return;
	        StartCoroutine(WaitABit());
	        return;
	    }
	    ScrollUp();
	}

    private void ScrollUp() {
        var pos = target.position;
        pos += target.transform.up * scrollSpeed * Time.deltaTime;
        target.transform.position = pos;
        currentDistance = Vector3.Distance(target.transform.position, originalPosition);
    }

    IEnumerator WaitABit() {
        isWaiting = true;

        yield return new WaitForSeconds(waitTime);

        while (true) {
            yield return new WaitForFixedUpdate();
            ScrollUp();
            if (currentDistance >= scrollLength + 200) {
                break;
            }
        }

        var resetpos = originalPosition;
        resetpos.y -= 400;
        currentDistance = 0;
        target.transform.position = resetpos;

        isWaiting = false;
    }
}
