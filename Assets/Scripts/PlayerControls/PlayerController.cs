﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class PlayerController : MonoBehaviour {

    private static bool didQueryNumOfCtrlrs = false;

    public XboxController controller;

    public MoveController moveController;
    public FireController fireController;
    public BulletController switchBulletController;

    public HealthController healthController;

    void Start() {
        healthController.onDestroy += (o, e) => {
            ControllerController.availablePlayers[controller] = false;
        };
    }

    void Update() {
        if (moveController) {
            moveController.Move(controller);
        }

        if (fireController) {
            fireController.Fire(controller);
        }

        if (switchBulletController) {
            switchBulletController.Switch(controller);
        }
    }
}
