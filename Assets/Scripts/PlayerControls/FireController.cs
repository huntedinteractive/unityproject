﻿using System;
using UnityEngine;
using XboxCtrlrInput;

public class FireController : MonoBehaviour {

    public BulletController bulletController;
    public float fireRate = 0.5f;
    private float lastShot = 0.0f;

    public void Fire(XboxController controller) {
        if (XCI.GetAxis(XboxAxis.RightTrigger, controller) > 0) {
            if (Time.time > fireRate + lastShot) {
                var selectedBullet = bulletController.GetSelectedBullet();
                switch (selectedBullet.name) {
                    case "LineBullet":
                    case "SineBullet":
                    case "SpiralBullet":
                        Instantiate(selectedBullet, transform.position, transform.rotation);
                        break;
                    case "HelixBullet":
                        Instantiate(selectedBullet, transform.position, transform.rotation);
                        Instantiate(selectedBullet, transform.position, transform.rotation);
                        break;
                }
                lastShot = Time.time;
            }
        }
    }
}
