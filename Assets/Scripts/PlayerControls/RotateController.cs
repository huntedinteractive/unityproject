﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class RotateController : MonoBehaviour {

    public void Rotate(XboxController controller) {
        var dir = new Vector3(XCI.GetAxisRaw(XboxAxis.RightStickX, controller) * -1, XCI.GetAxisRaw(XboxAxis.RightStickY, controller) * -1, 0);
        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, Angle(dir)));
    }

    float Angle(Vector3 a) {
        return Mathf.Atan2(a.y, a.x) * Mathf.Rad2Deg;
    }
}
