﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class BulletController : MonoBehaviour {

    public List<GameObject> availableBullets = new List<GameObject>();
    private int selectedIndex = 0;

    public void Switch(XboxController controller) {
        if (XCI.GetButtonDown(XboxButton.RightBumper)) {
            PickNextBullet();
        } else if (XCI.GetButtonDown(XboxButton.LeftBumper)) {
            PickPreviousBullet();
        }
    }

    public GameObject GetSelectedBullet() {
        return availableBullets[selectedIndex];
    }

    private void PickPreviousBullet() {
        if (--selectedIndex < 0)
            selectedIndex = availableBullets.Count - 1;
    }

    private void PickNextBullet() {
        if (++selectedIndex > availableBullets.Count - 1)
            selectedIndex = 0;
    }

    public void Add(GameObject bullet) {
        availableBullets.Add(bullet);
    }
}
