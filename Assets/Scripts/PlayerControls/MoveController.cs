﻿using UnityEngine;
using XboxCtrlrInput;

public class MoveController : MonoBehaviour {

    public float speed = 100.0f;
    public float rotationSpeed = 500.0f;
    public float friction = 0.5f;
    private float velocity;

    public void Move(XboxController controller) {
        var direction = XCI.GetAxis(XboxAxis.LeftStickX, controller);
        var thrust = XCI.GetAxis(XboxAxis.LeftStickY, controller);
        thrust = thrust < 0 ? 0 : thrust;

        transform.Rotate(direction * Vector3.back * rotationSpeed * Time.deltaTime);

        velocity += thrust * speed * Time.deltaTime;
        velocity *= friction;

        transform.position += transform.up * velocity * -1;

    }
}
