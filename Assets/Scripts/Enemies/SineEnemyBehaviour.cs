﻿using UnityEngine;

public class SineEnemyBehaviour : BaseEnemyBehaviour {
    private Vector3 currentPosition;
    private float sineAxis;
    private float startTime;

    public float frequency = 5;
    public float magnitude = 5;

    public override void Start() {
        base.Start();
        startTime = Time.time;

        currentPosition = transform.position;
    }

    public override void MoveToTarget() {

        var perp = Vector3.Cross(heading, Vector3.forward) * Mathf.Cos((startTime - Time.time) * frequency) * magnitude;
        if (!shouldRotate) perp = Vector3.zero;

        currentPosition += (heading + perp).normalized * Time.deltaTime * speed;
        transform.position = currentPosition;

        if (!shouldRotate) return;
        var rotation = Quaternion.LookRotation((heading+ perp).normalized, Vector3.forward);
        rotation.x = rotation.y = 0;
        transform.rotation = rotation;
    }
}