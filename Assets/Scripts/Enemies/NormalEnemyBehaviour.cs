﻿using UnityEngine;

public class NormalEnemyBehaviour : BaseEnemyBehaviour {

    public override void MoveToTarget() {
        var step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, step);
        if (!shouldRotate) return;
        var rotation = Quaternion.LookRotation(transform.position - target, -Vector3.forward);
        rotation.x = rotation.y = 0;
        transform.rotation = rotation;

    }
}