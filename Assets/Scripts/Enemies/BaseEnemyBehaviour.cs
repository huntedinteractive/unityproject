﻿using System.Collections;
using UnityEngine;

public abstract class BaseEnemyBehaviour : MonoBehaviour {
    public float speed;
    public Animator animator;
    public new Collider2D collider;
    protected bool shouldRotate = true;
    internal Vector3 heading;


    protected Vector3 target;
    public virtual void Start() {
        target = Vector3.zero;
        heading = (target - transform.position).normalized;
    }

    private void Update() {
        MoveToTarget();
    }

    public virtual void Init() {}

    public void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "UFO" || coll.gameObject.tag == "Player") {
            DestroyObject();
        }
    }

    public void OnTriggerEnter2D(Collider2D coll) {
        if (coll.tag != "Bullet") return;
        DestroyObject();

        var cc = coll.GetComponent<ComboController>();
        if (cc != null && cc.GetComboCounting() > 0 ) {
            cc.SubstractCounting();
            return;
        }
        Destroy(coll.gameObject);

    }

    private void DestroyObject() {
        shouldRotate = false;

        collider.enabled = false;
        var r = Random.Range(1, 3);
        animator.SetTrigger("Destroyed" + r);
    }

    public void OnTriggerExit2D(Collider2D coll) {
        if (coll.tag == "Bounds")
            DestroyObject();
    }

    public abstract void MoveToTarget();
}
