﻿using System.Linq;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    public Transform from;
    public Transform to;


    public Vector3 GetRandomPosition() {
        var time = Random.value;
        return Vector3.Lerp(from.position, to.position, time);
    }

}