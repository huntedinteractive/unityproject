﻿using System;
using UnityEngine;

public class SpiralBulletPattern : BaseBulletPattern {
    public float divider = 20f;
    public float incrementer = 0.09f;

    public override void Start() {
        base.Start();
    }

    public override void UpdateBullet() {
        var perp = Vector3.Cross(heading, Vector3.forward);
        perp = perp / divider;

        divider += incrementer;
        heading = heading + perp;

        currentPosition += heading.normalized * Time.deltaTime * speed;
        transform.position = currentPosition;
    }
}
