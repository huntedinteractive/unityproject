﻿using System;
using UnityEngine;

public class SineBulletPattern : BaseBulletPattern {
    public float frequency;
    public float magnitude;

    private float startTime;

    public override void Start() {
        base.Start();
        startTime = Time.time;
    }

    public override void UpdateBullet() {
        var perp = Vector3.Cross(heading, Vector3.forward) * Mathf.Cos((startTime - Time.time) * frequency) * magnitude;
        currentPosition += (heading + perp).normalized * Time.deltaTime * speed;
        transform.position = currentPosition;
    }
}
