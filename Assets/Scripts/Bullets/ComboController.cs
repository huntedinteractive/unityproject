﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboController : MonoBehaviour {

    public int comboCounter;

    public int GetComboCounting() {
        return comboCounter;
    }

    public void SubstractCounting() {
        comboCounter--;
    }
}
