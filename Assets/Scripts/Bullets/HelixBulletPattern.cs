﻿using System;
using UnityEngine;

public class HelixBulletPattern : BaseBulletPattern {
    public float frequency;
    public float magnitude;

    private float startTime;

    private float sineAxis;
    private static bool flip = true;

    public override void Start() {
        base.Start();
        startTime = Time.time;
        sineAxis = flip ? 1 : -1;
        flip = !flip;
    }

    public override void UpdateBullet() {
        var perp = Vector3.Cross(heading, Vector3.forward * sineAxis).normalized * Mathf.Cos((startTime - Time.time) * frequency) * magnitude;
        currentPosition += (heading + perp).normalized * Time.deltaTime * speed;
        transform.position = currentPosition;
    }
}
