﻿using UnityEngine;

public class StraightBulletPattern : BaseBulletPattern {

    public override void UpdateBullet() {
        currentPosition += heading * Time.deltaTime * speed;
        transform.position = currentPosition;
    }
}