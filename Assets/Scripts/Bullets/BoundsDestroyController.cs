﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsDestroyController : MonoBehaviour {
    private float leftConstraint;
    private float rightConstraint;
    private float bottomConstraint;
    private float topConstraint;
    private float distanceZ;
    private float buffer = 0.0f;

    void Start() {
        var cam = Camera.main;
        distanceZ = Mathf.Abs(cam.transform.position.z + transform.position.z);

        leftConstraint = cam.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, distanceZ)).x;
        rightConstraint = cam.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, distanceZ)).x;
        bottomConstraint = cam.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, distanceZ)).y;
        topConstraint = cam.ScreenToWorldPoint(new Vector3(0.0f, Screen.height, distanceZ)).y;
    }

    void FixedUpdate() {
        if (transform.position.x < leftConstraint - buffer ||
            transform.position.x > rightConstraint + buffer ||
            transform.position.y < bottomConstraint - buffer ||
            transform.position.y > topConstraint + buffer) {
            Destroy(gameObject);
        }
    }
}
