﻿using System;
using UnityEngine;
public abstract class BaseBulletPattern : MonoBehaviour {
    public Vector3 heading;
    public float speed;
    public Sprite sprite;

    protected Vector3 currentPosition;

    public virtual void Start() {
        heading = transform.rotation * Vector3.down;
        currentPosition = transform.position + heading * 0.5f;
    }

    void Update() {
        UpdateBullet();
    }

    public abstract void UpdateBullet();
}