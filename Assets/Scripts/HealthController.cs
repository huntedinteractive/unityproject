﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class HealthController : MonoBehaviour {

    public float health;
    public float damage;

    public Animator animator;
    public new Collider2D collider;

    public EventHandler onDestroy;

    private void DoDamage() {
        health -= damage;
        if(health <= 0f) {
            DestroyObject();
            return;
        }
        if (tag == "UFO") {
            animator.SetTrigger("Damaged");
        }
    }

    public void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "Enemy") {
            DoDamage();
        }
    }

    public void DestroyObject() {
        collider.enabled = false;
        var r = Random.Range(1, 3);
        animator.SetTrigger("Destroyed"+r);

        var od = onDestroy;
        if (od != null)
            od(this, null);
    }
}
