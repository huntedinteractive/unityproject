﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using XboxCtrlrInput;

public class GameOverController : MonoBehaviour {
    public float waitTime;
    public Text startText;

    private bool canSkip;
    public void Start() {
        startText.enabled = false;
        ControllerController.Reset();
        StartCoroutine(Wait());
    }

    private IEnumerator Wait() {
        yield return new WaitForSeconds(waitTime);
        canSkip = true;
        startText.enabled = true;
    }


    void Update () {
        if (!canSkip) return;

        if (XCI.GetButtonDown(XboxButton.Start)) {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
