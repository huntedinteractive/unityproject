﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {
    public int playerNumber;
    public int health;
    public BaseBulletPattern currentPattern;
    public BaseBulletPattern[] availablePatterns;

    public EventHandler onHealthChanged;
    public EventHandler onPatternChanged;

    public void Start() {
        currentPattern = availablePatterns[0];
    }

    public void Update() {
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            currentPattern = availablePatterns[0];
            onPatternChanged(this, null);
        }else if (Input.GetKeyDown(KeyCode.Alpha2)) {
            currentPattern = availablePatterns[1];
            onPatternChanged(this, null);
        }else if (Input.GetKeyDown(KeyCode.Equals)) {
            if (health < 3) {
                health++;
            }
            onHealthChanged(this, null);
        } else if (Input.GetKeyDown(KeyCode.Minus)) {
            if (health > 0) {
                health--;
            }
            onHealthChanged(this, null);
        }

    }
}
