﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using XboxCtrlrInput;

public class ControllerController : MonoBehaviour {
    public static Dictionary<XboxController, bool> availablePlayers;
    private static bool _woke;
    private static bool _startGame;

    public HealthController ufoHealthController;

    void Awake() {
        if (_woke) return;
        availablePlayers = new Dictionary<XboxController, bool> {
            {XboxController.First, false },
            {XboxController.Second, false },
            {XboxController.Third, false },
            {XboxController.Fourth, false },
        };
        _startGame = false;
        _woke = true;
        _gameover = false;
        Application.targetFrameRate = 60;

    }

    public static void Reset() {
        _woke = false;
    }

    private static bool _gameover;
    void Update() {
        if (_gameover) return;
        if (_startGame) {
            if (availablePlayers.All(a => a.Value == false)) {
                ufoHealthController.DestroyObject();
                _gameover = true;
            }
        }

        if (!availablePlayers.Any(l => l.Value)) return;
        if (XCI.GetButtonDown(XboxButton.Start))
            StartGame();
    }

    private void StartGame() {
        SceneManager.LoadScene("BaseLevel");
        _startGame = true;
    }
}
