﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnController : MonoBehaviour {
    public EnemySpawner[] spawners;
    public GameObject[] prefabs;
    public Transform target;

    public float spawnTime;
    public float spawnRate;

    void Start() {
        keepSpawning = true;
        StartCoroutine(SpawnCoroutine());
    }

    private bool keepSpawning;

    IEnumerator SpawnCoroutine() {
        while (keepSpawning) {
            yield return new WaitForSeconds(spawnTime);
            for (var i = 0; i < spawnRate; i++) {
                Spawn();
            }
        }
    }

    void OnDestroy() {
        keepSpawning = false;
    }

    void Spawn() {
        var prefab = GetRandomFromCollection(prefabs);
        var position = GetRandomFromCollection(spawners).GetRandomPosition();
        Instantiate(prefab, position, Quaternion.identity);
    }

    private static T GetRandomFromCollection<T>(IEnumerable<T> i) {
        var r = Random.Range(0, i.Count());
        return i.ElementAt(r);
    }


}
