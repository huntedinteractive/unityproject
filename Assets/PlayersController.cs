﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersController : MonoBehaviour {
    public PlayerController[] players;

    void Awake() {
        foreach (var p in players) {
            p.gameObject.SetActive(ControllerController.availablePlayers[p.controller]);
        }
    }
}
