﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using XboxCtrlrInput;

public class PlayerSelectController : MonoBehaviour {

    public XboxController controller;
    public RawImage image;
    private bool isSelected;

	void Start () {
	    image.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	    if (isSelected || !XCI.GetButtonDown(XboxButton.A, controller)) return;
	    Select();
	}

    private void Select() {
        isSelected = true;
        image.enabled = true;
        ControllerController.availablePlayers[controller] = true;
    }
}
